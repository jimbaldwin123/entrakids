<?php

require 'classes/phpmailer/PHPMailerAutoload.php';
// require 'classes/maropost/vendor/autoload.php';

// if( $_POST['1234asf1'] != '' ){

require_once('../dbinc/dbinc.php');
$db = new PDO('mysql:host=DB_HOST;dbname=entrakids_submissions;charset=utf8', DB_USER, DB_PASSWORD);

$stmt = $db->prepare("INSERT INTO entrakids (fname, lname, email, password, country, state) VALUES (:fname, :lname, :email, :password, :country, :state)");
$stmt->bindParam(':fname', $fname);
$stmt->bindParam(':lname', $lname);
$stmt->bindParam(':email', $email);
$stmt->bindParam(':password', $password);
$stmt->bindParam(':country', $country);
$stmt->bindParam(':state', $state);

$fname    = $_POST['fname'];
$lname    = $_POST['lname'];
$email    = $_POST['email'];
$password = $_POST['password'];
$country  = $_POST['country'];
$state    = $_POST['state'];
// $id       = time(); // using autoincrement instead

$body = "First Name: " . $fname . '<br>';
$body .= "Last Name: " . $lname . '<br>';
$body .= "Email: " . $email . '<br>';
$body .= "Password: " . $password . '<br>';
$body .= "Country: " . $country . '<br>';
$body .= "State: " . $state . '<br>';
// $body .= "ID: " . $id . '<br>';

/*

Create table 'entrakids'

CREATE TABLE `entrakids` (
 `db_id` INT NOT NULL AUTO_INCREMENT ,
 `fname` VARCHAR(255) NOT NULL ,
 `lname` VARCHAR(255) NOT NULL ,
 `email` VARCHAR(255) NOT NULL ,
 `password` VARCHAR(255) NOT NULL ,
 `country` VARCHAR(255) NOT NULL ,
 `state` VARCHAR(255) NOT NULL ,
 `id` INT UNSIGNED NOT NULL ,
 PRIMARY KEY (`db_id`)) ENGINE = InnoDB;

 */

/* Maropost record

{"id":45536279,
"email":"sheba030486@gmail.com",
"first_name":"Sheba Marie",
"last_name":"Alianza",
"phone":"09162149574",
"fax":null,
"created_at":"2014-08-28T18:28:55.000-04:00",
"updated_at":"2016-02-01T21:37:29.000-05:00",
"aid":"358906",
"uid":"119770",
"username":"sheba030486682",
"password":"nokia3610",
"activation_code":"c404b716a5f31b0f838d0f4168351a1211f6136f378e1b49c5561d47dba6b8b3",
"eid":"bim",
"country":"PH",
"state":"Davao del Sur",
"account_id":211,
"subscription":{"status":"Subscribed",
"subscribed_at":"2016-02-01T21:47:40.000-05:00",
"updated_at":"2016-02-01T21:47:40.000-05:00"},
"total_pages":1}
*/

class MP {
    public function __construct($acct, $auth, $format = 'json')
    {
        $this->auth = $auth;
        $this->acct = $acct;
    }
    static $url_api 	= "https://api.socketlabs.com/marketing/v1/";

	function request($action, $endpoint, $dataArray) {
		$url = self::$url_api . $endpoint;
	  	$ch = curl_init();
	  	// $dataArray['auth_token'] = self::$auth_token;
	  	$json = json_encode($dataArray);
        print $json . "\n";
	    //curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_USERPWD, $this->acct . ':' . $this->auth);
	    curl_setopt($ch, CURLOPT_MAXREDIRS, 10 );
	    curl_setopt($ch, CURLOPT_URL, $url);
	    switch($action){
	            case "POST":
	            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
	            break;
	        case "GET":
	            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
	            break;
	        case "PUT":
	            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
	            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
	            break;
	        case "DELETE":
	            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
	            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
	            break;
	        default:
	            break;
	    }
	    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json','Accept: application/json'));
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
	    $output = curl_exec($ch);
	    curl_close($ch);
	    $decoded = json_decode($output);
	    return $decoded;
	}
}
// EXAMPLE 1: Add new contact to SocketLabs:
$mp = new MP('14627', 'Zk7o3A6SqTw4i8Q2FgBf');

$newcontact = $mp->request('PUT','lists/5524/contacts', array(
		'firstName'  => $fname,
		'lastName'   => $lname,
		'emailAddress' 	  => $email
	));


try {

    $stmt->execute();

} catch(PDOException $ex) {
    //Something went wrong rollback!
    $db->rollBack();
    echo 'ERROR';
}

$mail = new PHPMailer;

//$mail->SMTPDebug = 3;                               // Enable verbose debug output

$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host       = 'smtp.gmail.com';  // Specify main and backup SMTP servers
$mail->SMTPAuth   = true;                               // Enable SMTP authentication
$mail->Username   = 'bcrist@mcclainconcepts.com';                 // SMTP username
$mail->Password   = 'YR3%AzIQ0&o#kPk';                           // SMTP password
$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
$mail->Port       = 587;                                    // TCP port to connect to

$mail->setFrom( 'bcrist@mcclainconcepts.com', 'Entra Kids Submission' );
$mail->addAddress( 'jbaldwin@mcclainconcepts.com', 'McClain Developer' );     // Add a recipient
// $mail->addBCC( 'info@entrakids.org', 'Entrakids Info' );
//$mail->addBCC( 'info@mcclainfoundation.org', 'Entrakids Info' );
/*$mail->addAddress('ellen@example.com');               // Name is optional
$mail->addReplyTo('info@example.com', 'Information');
$mail->addCC('cc@example.com');
$mail->addBCC('bcc@example.com');*/

/*$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name*/
$mail->isHTML( true );                                  // Set email format to HTML

$mail->Subject = 'New EntraKids Contact Submission';
$mail->Body    = $body;
/*$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';*/

if ( ! $mail->send()) {
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' .

         $data['result'] = "fail";
    $data['message'] = $mail->ErrorInfo;

    print_r( json_encode( $data ) );
    die();
} else {


	
    $data['result']  = "success";
    $data['message'] = "Message has been sent";
    $data['userid']  = $newcontact->id;

    print_r( json_encode( $data ) );
    die();
}
//}
