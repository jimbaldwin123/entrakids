$().ready(function() {

		// validate signup form on keyup and submit
		$("#applyForm").validate({
			rules: {
				fname: "required",
				lname: "required",
				password: {
					required: true,
					minlength: 5
				},
				passwordc: {
					required: true,
					minlength: 5,
					equalTo: "#password"
				},
				email: {
					required: true,
					email: true
				},
				country: {
					required: true,
				},
				state: {
					required: true,
				},
			},
			messages: {
				firstname: "Please enter your firstname",
				lastname: "Please enter your lastname",
				username: {
					required: "Please enter a username",
					minlength: "Your username must consist of at least 2 characters"
				},
				password: {
					required: "Please provide a password",
					minlength: "Your password must be at least 5 characters long"
				},
				confirm_password: {
					required: "Please provide a password",
					minlength: "Your password must be at least 5 characters long",
					equalTo: "Please enter the same password as above"
				},
				email: "Please enter a valid email address",
				agree: "Please accept our policy"
			}
		});

$("#applyFormVip").validate({
			rules: {
				mobile: "required",
				userid: "required",
				country2: "required"
			},
			messages: {
				mobile: "Please enter your mobile",
				userid: "Please enter your userid",
				country2: "Please enter your country",
			}
		});

	$("#applyForm").submit(function(e) {

		if ($("#applyForm").valid()){

		    var url = "apply.php"; // the script where you handle the form input.

		    $.ajax({
		           type: "POST",
		           url: url,
		           data: $("#applyForm").serialize(), // serializes the form's elements.
		           success: function(data)
		           {
		           		data = jQuery.parseJSON(data);
						if(data.result == 'success'){
							$('#userid').val(data.userid);
							$('#complete1').val('1');
			               	$(".modal01-close").click();

			            	$('#Modal01').on('hidden.bs.modal', function (e) {
                                                /*.modal02 changed to .modal03 to skip second modal CHANGEDMODAL*/
			            		$('.modal03').click();
							});
						}
		           }
		         });

		    e.preventDefault(); // avoid to execute the actual submit of the form.


		} else {
			$('#Modal01').modal('handleUpdate');
		}
	});

$("#applyFormVip").submit(function(e) {

		if ($("#applyFormVip").valid()){

		    var url = "applyVip.php"; // the script where you handle the form input.

		    $.ajax({
		           type: "POST",
		           url: url,
		           data: $("#applyFormVip").serialize(), // serializes the form's elements.
		           success: function(data)
		           {
		           		data = jQuery.parseJSON(data);
						if(data.result == 'success'){
			               	$(".modal02-close").click();

			            	$('#Modal02').on('hidden.bs.modal', function (e) {
			            		$('.modal03').click();
							});
						}
		           }
		         });

		    e.preventDefault(); // avoid to execute the actual submit of the form.


		} else {
			$('#Modal02').modal('handleUpdate');
		}
	});

});
