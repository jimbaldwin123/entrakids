<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

	<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <title>Entralife</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/grayscale.css" rel="stylesheet">
    
    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script src="https://use.typekit.net/bcs5lch.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>
</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
    <!-- Intro Header -->
    <!-- About Section -->
    <section id="about" class="about-text">
    <div class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2" >
                <h2>THANK YOU</h2>
                <p>FOR REQUESTING ACCESS TO ENTRALIFE</p>
                <p>Your application is received and is currently pending.  If and when your application is approved you will be notified via email.</p>
                <a href="http://entralife.com" class="btn btn-default btn-lg">BACK TO ENTRALIFE.COM</a>
            </div>
        </div>
    </div>    
    </section>
    <!-- Footer -->
    <footer>
        <div class="container text-center">
       	  <div class="row">
            <div class="text-center foot-img">
                    <img class="img-responsive center-block" alt="Entralife Logo" width="181.594" height="22.643"  src="http://entralife.com/img/logo.png" />
                    </div>
                </div>
            <p>Terms &amp; Privacy Policy DMCA Notice Code of Conduct<br />
            &copy; 2016 - Entralife.com - All Rights Reserved.</p><br />
            <p>Entralife, LLC 30161-C Avenida de las Banderas, Suite C, Rancho Santa Margarita, CA 92688</p>
        </div>
    </footer><!-- jQuery -->

    <script src="js/jquery.js"></script> 

    <script src="js/jquery.validate.min.js"></script> 

    <script src="js/country-region-selector/dist/crs.js"></script> 

    <script src="js/bootstrap-select.js"></script> <!-- Bootstrap Core JavaScript -->
     
    <script src="js/bootstrap.min.js"></script> <!-- Plugin JavaScript -->
     
    <script src="js/jquery.easing.min.js"></script>

    <script src="js/main.js"></script>

    <?php /*
     <!-- Google Maps API Key - Use your own API key to enable the map feature. More information on the Google Maps API can be found at https://developers.google.com/maps/ -->
     <!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRngKslUGJTlibkQ3FkfTxj3Xss1UlZDA&sensor=false"></script> -->
     <!-- Custom Theme JavaScript --> */
     
    <script src="js/grayscale.js"></script>

    <style>
        label.error {color:red;}
    </style>
</body>
</html>